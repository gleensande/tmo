#include "rnd_gen.hpp"

// рандом
// random_device generator;
// детерминированный рандом
default_random_engine generator;

// генерация случайого равномерно распределенного
// числа с плавающей точкой в диапазоне [a, b)
double rnd_uniform(double a, double b) {
    uniform_real_distribution<double> distribution(a, b);
    
    return distribution(generator);
}

// генерация случайого экспоненциально распределенного
// числа с плавающей точкой по параметру lamb (λ) - среднее время появления
double rnd_exponential(double lamb) {
    exponential_distribution<double> distribution(lamb);

    return distribution(generator);
}