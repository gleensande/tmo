#ifndef TMO_RND_GEN_HPP
#define TMO_RND_GEN_HPP

#include <random>
using namespace std;


double rnd_uniform(double a, double b);
double rnd_exponential(double lamb);


#endif  // TMO_RND_GEN_HPP
