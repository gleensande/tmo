#ifndef TMO_EVENT_HPP
#define TMO_EVENT_HPP


#define Q 0
#define END 1

#include <iostream>
using namespace std;

struct Event {
    int transact_num;
    double move_time;
    bool next;

    Event(int tn, double mt, bool nxt):  transact_num(tn), move_time(mt), next(nxt) {};
    void print();
};


#endif  // TMO_EVENT_HPP