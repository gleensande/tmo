set terminal png
set output 'results/channels.png'

set xlabel 'Количество занятых каналов'
set ylabel 't/T_{max}'

set boxwidth 0.8
set style fill solid

plot "results/channels.dat" using 1:2:xtic(1) with boxes title '' linecolor rgb "#628395", \
    "results/channels.dat" using 1:($2+0.02):2 with labels rotate by 90 title ''



set output 'results/queues.png'

set xlabel 'Количество занятых мест в очереди'
set ylabel 't/T_{max}'

set boxwidth 0.8
set style fill solid

plot "results/queues.dat" using 1:2:xtic(1) with boxes title '' linecolor rgb "#628395", \
    "results/queues.dat" using 1:($2+0.02):2 with labels rotate by 90 title ''