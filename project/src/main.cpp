#include <iostream>
#include <set>
#include <queue>
#include <fstream>
#include <iomanip>
#include "event.hpp"
#include "rnd_gen.hpp"

using namespace std;

// константы из задания
double Tc = 36;             // 1 / Tc = lamb (λ) - среднее время появления заявок
double Ts = 217;            // 1 / Ts = mu (μ) – интенсивность обслуживания заявок
double T_max = 10e4;        // время работы системы

// устройство
int n = 12;                 // количество каналов устройства
int n_busy = 0;             // количество занятых каналов устройства

// статистика
vector<double> chanels_times(n + 1, 0);
vector<double> q_times;

double prev_chanel_time = 0;
double prev_q_time = 0;

// цепочки событий упорядоченные по времени
struct EventCmp {
    bool operator()(const Event& lhs, const Event& rhs) const { 
        return (lhs.move_time < rhs.move_time); 
    }
};

set<Event, EventCmp> CEC;
set<Event, EventCmp> FEC;


// номер последнего созданного транзакта
int cur_transact_num = 0;

// текущее модельное время
double cur_time = 0;
double prev_event_gen_time = 0;

// очередь из транзактов
queue<int> q;

// функция генерации событий
void generate_events(int n) {
    for (int i = 0; i < n; i++) {
        double time = rnd_uniform(0, 2 * Tc);
        Event cur_event(cur_transact_num, prev_event_gen_time + time, Q);
        FEC.insert(cur_event);
        cur_transact_num++;
        prev_event_gen_time += time;
    }
}

// перенос ближайшего события из FEC в CEC
void FEC_to_CEC() {
    CEC.insert(*FEC.begin());
    cur_time = FEC.begin()->move_time;
    FEC.erase(FEC.begin());
}

// печать цепочек
void print_FEC_CEC() {
    cout << endl;
    cout << "FEC" << endl;
    for (Event k : FEC) {
        k.print();
    }
    cout << endl << "CEC" << endl;
    for (Event k : CEC) {
        k.print();
    }
    cout << endl << "----------------" << endl;
}

// перенос из CEC в очередь
void CEC_to_Q() {
    q.push(CEC.begin()->transact_num);
    CEC.erase(CEC.begin());
}

// занятие канала в устройстве по номеру транзакта
void enter_device() {
    if (q.size() == 0) {
        return;
    }
       
    int transact_num = q.front();
    q.pop();

    n_busy++;

    double time = rnd_uniform(0, 2 * Ts);
    Event cur_event(transact_num, cur_time + time, END);
    FEC.insert(cur_event);
}

// перенос из CEC на смерть 
void CEC_to_END() {
    n_busy--;
    CEC.erase(CEC.begin());
    return;
}

int main() {
    while (cur_time < T_max) {
        // 1. генерация события
        generate_events(1);

        // 2. перенос ближайшего события из FEC в CEC
        FEC_to_CEC();

        // 3. перенос ближайшего события из CEC в Q или смерть
        chanels_times[n_busy] += cur_time - prev_chanel_time;
        prev_chanel_time = cur_time;
        switch (CEC.begin()->next) {
            case(Q):
                if (q.size() == q_times.size()) {
                    q_times.push_back(cur_time - prev_q_time);
                } else {
                    q_times[q.size()] += cur_time - prev_q_time;
                }
                prev_q_time = cur_time;
                CEC_to_Q();
                // 4. попытка занять девайс первым в очереди транзактом
                if (n_busy < n) {
                    enter_device();
                }
                break;
            case(END): 
                CEC_to_END();
                enter_device(); 
                break;
        }

    }

    // запись статистики в файлы
    ofstream channels_file;
    channels_file.open("results/channels.dat");
    if (!channels_file) {
        cout << "ОШИБКА: невозможно открыть файл результатов для каналов" << endl;
        return 1;
    }
    
    channels_file.precision(2);
    for (size_t i = 0; i < chanels_times.size(); i++) {
        channels_file << i << "\t" << chanels_times[i] / cur_time << endl;
    }

    ofstream q_file;
    q_file.open("results/queues.dat");
    if (!q_file) {
        cout << "ОШИБКА: невозможно открыть файл результатов для очередей" << endl;
        return 1;
    }

    q_file.precision(2);
    for (size_t i = 0; i < q_times.size(); i++) {
        q_file << i << "\t" << q_times[i] / cur_time << endl;
    }
}